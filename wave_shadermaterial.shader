shader_type canvas_item;
uniform vec3 iResolution; 
uniform vec2 iPos;
uniform vec2 CIRCLE_CENTER = vec2(0.9, 0.5);
uniform float MAX_RADIUS = 0.5;

float circleFast(vec2 st, vec2 pos, float r) {
    vec2 dist = st-pos;
    return 1.-smoothstep(r-(0.09),r+(0.09),dot(dist,dist)*4.);
}

void fragment()
{
    float time = fract(TIME/2.);
	vec2 uv = SCREEN_UV; // iResolution.y; // ( FRAGCOORD.xy -.5*iResolution.xy ) / iResolution.y;
	
	vec4 col = vec4(uv,0.5+0.5*sin(TIME),1.0);
    uv += 1.-(circleFast(uv, CIRCLE_CENTER, time*2.)-circleFast(uv, CIRCLE_CENTER, (time/1.3)*2.))
    * (1.-clamp(distance(CIRCLE_CENTER, uv)*1./MAX_RADIUS,0.,1.));
    col = texture(SCREEN_TEXTURE, uv);
	COLOR = mix(col, vec4(0,0,0,1), smoothstep(0.01, 1.01, 0.2));
}
