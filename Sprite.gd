extends ViewportContainer

onready var mouse_pos = material.get_shader_param("iMouse")
onready var viewport = get_node("Viewport")

# Called when the node enters the scene tree for the first time.
func _ready():
	var ws = get_viewport_rect().size
	viewport.size = ws
	self.set("rect_size", ws)
	self.material.set_shader_param("iResolution", ws)
	set_process(true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#Vector2(get_local_mouse_position().x, get_local_mouse_position().y)
	self.material.set_shader_param("iPos", Vector2(get_local_mouse_position().x, get_local_mouse_position().y))
	pass
