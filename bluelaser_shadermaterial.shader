shader_type canvas_item;

void fragment(){
	vec2 uv = SCREEN_UV;
    
    float y = mod(-TIME / 10., 1.9) - 0.4;
	
    float str = -pow((uv.y - y) * 110., 2.) + .8;
    uv.x -= clamp(str * .01, 0., 1.);
    COLOR = texture(SCREEN_TEXTURE, uv);
    
    float colorAdd = pow(1. - pow(abs(uv.y - y), .3), 3.);
    COLOR.g += colorAdd * 0.5;
    COLOR.b += colorAdd * 1.;
}