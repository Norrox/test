shader_type canvas_item;

uniform float dampening = 400.0;

void fragment()
{
	//vec2 iResolution = vec2(100,100);
    //Sawtooth function to pulse from centre.
	//vec2 diff = vec2(0.5, 0.5) + vec2(UV.x, 1.0-UV.y);
	//float d = length(diff)*2.0;
	//vec2 dir = normalize(diff);
    float offset = (TIME- floor(TIME))/TIME;
	float CurrentTime = (TIME)*(offset);    
    
	vec3 WaveParams = vec3(15.0, 1.8, 0.1 ); 
    
    //float ratio = iResolution.y/iResolution.x;
    
    //Use this if you want to place the centre with the mouse instead
	//vec2 WaveCentre = vec2( iPos.xy / iResolution.xy );
	//WaveCentre.y *= -1.0;
       
    vec2 WaveCentre =  UV + SCREEN_UV - vec2(0.5, 0.5);
	//WaveCentre.y *= -0.5; 
    //WaveCentre.y *= ratio; 
   
	//float f = clamp(1.0-d, 0.0, 1.0);
	
	// This is a 0..1 value that will nullify the effect around the bounds of the effect,
	// for a seamless transition between the effect's area and the unaffected world pixels.
	//float shelf = smoothstep(0.0, 1.0, f);
	
	// Calculate displacement amount
	//float displacement = strength / (d*d + 0.01);
	
	// Calculate distorted screen-space texture coordinates
	//vec2 uv = SCREEN_UV + dir * (displacement * shelf);


	vec2 texCoord = SCREEN_UV;//+ (displacement * shelf);// + dir * (displacement * shelf); // iResolution.xy;      
    //texCoord.y *= ratio;  
	//texCoord.y = 0.5 - texCoord.y; //This almost work for where the effect is created
	//texCoord.y *= -1.0;// - texCoord.y;  //this makes the effectto be at the bottom of the screen
	float Dist = distance(texCoord, WaveCentre);
    
	
	vec4 Color = texture(SCREEN_TEXTURE, texCoord);
    
//Only distort the pixels within the parameter distance from the centre
if ((Dist <= ((CurrentTime) + (WaveParams.z))) && 
	(Dist >= ((CurrentTime) - (WaveParams.z)))) 
	{
        //The pixel offset distance based on the input parameters
		float Diff = (Dist - CurrentTime); 
		float ScaleDiff = (1.0 - pow(abs(Diff * WaveParams.x), WaveParams.y)); 
		float DiffTime = (Diff  * ScaleDiff);
        
        //The direction of the distortion
		vec2 DiffTexCoord = normalize(texCoord - WaveCentre);         
        
        //Perform the distortion and reduce the effect over time
		texCoord += ((DiffTexCoord * DiffTime) / (CurrentTime * Dist * dampening));
		Color = texture(SCREEN_TEXTURE, texCoord);
        
        //Blow out the color and reduce the effect over time
		Color += (Color * ScaleDiff) / (CurrentTime * Dist * dampening);
	} 
    
	COLOR = Color; 
}